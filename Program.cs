﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using DynamoDBQueryConsole.Model;

namespace DynamoDBQueryConsole
{
    class Program
    {
        private static readonly string dbname = "Customer";
        private static readonly AmazonDynamoDBClient client = new AmazonDynamoDBClient("AKIA2ABZ2VPRIEQF4IEY", "frIXrbbtSmZa9dg88rNnhgudrQFhUb0SYBFi907Q", RegionEndpoint.USEast1);

        static void Main(string[] args)
        {
            string query;
            Console.Write("Enter Last Name (example - Kizer, Smith, Cortez, Tang): ");
            query = Console.ReadLine();

            try
            {
                var Customers = Task.Run(() => GetList(query)).GetAwaiter().GetResult();
                Customers.ForEach(x => Console.WriteLine(x.FirstName + " " + x.LastName));

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static async Task<List<Customer>> GetList(string query)
        {
            var customers = new List<Customer>();

            using (var dbContext = new DynamoDBContext(client))
            {
                customers = await dbContext.QueryAsync<Customer>(query, new DynamoDBOperationConfig { OverrideTableName = dbname, IndexName = "LastName-index" }).GetRemainingAsync();
            }

            return customers;
        }
    }
}
